## Setup

```bash
pyenv install 3.9
pyenv virtualenv 3.9 product_manager
pyenv local product_manager
pip install -r requirements.txt
```

## Set up your environment variables

- Copy `cp .env.example .env`
- Set up variables in` .env`

## Run locally postgresql and rabbitmq

```bash
docker-compose up -d postgres rabbitmq
```

Run Django application:

```bash
python manage.py migrate
python manage.py runserver
```

## Run in docker compose

todo

