from django.apps import AppConfig


class SellerConfig(AppConfig):
    name = 'apps.seller'
